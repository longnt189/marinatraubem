const sm = require('sitemap');

const sitemap = sm.createSitemap({
    hostname: 'https://citya.vn',
    cacheTime: 600000,
    urls: [
        { url: '/', changefreq: 'daily', priority: 0.8 },

        { url: '/vi/cho-thue', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/biet-thu-villa', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/can-ho-cao-cap', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/nha-pho', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/phong', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/mat-bang', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/phong-le', changefreq: 'daily', priority: 0.8 },

        { url: '/vi/cho-thue/khu-vuc-hai-chau', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/khu-vuc-ngu-hanh-son', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/khu-vuc-thanh-khe', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/khu-vuc-son-tra', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/khu-vuc-lien-chieu', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/khu-vuc-cam-le', changefreq: 'daily', priority: 0.8 },
        { url: '/vi/cho-thue/khu-vuc-hoi-an', changefreq: 'daily', priority: 0.8 },

        { url: '/vi/tin-tuc', changefreq: 'daily', priority: 0.7 },
        { url: '/vi/tin-tuc/chuyen-muc-tin-tuc', changefreq: 'daily', priority: 0.7 },
        { url: '/vi/tin-tuc/chuyen-muc-kinh-nghiem', changefreq: 'daily', priority: 0.7 },

        { url: '/vi/tuyen-dung', priority: 0.3},

        { url: '/vi/gui-yeu-cau/cho-thue-bat-dong-san' },
        { url: '/vi/gui-yeu-cau/tim-bat-dong-san-cho-thue' },
        { url: '/vi/gui-yeu-cau/lien-he-gop-y' }
    ]
});

//=========== AUTO GENERATOR SITEMAP ============//

// const SitemapGenerator = require('sitemap-generator');

// const generator = SitemapGenerator('http://localhost:3000', {
//     stripQuerystring: false
// });

// generator.on('error', (error) => {
//     console.log('> Error:', error);
// });

// generator.on('ignore', (url) => {
//     console.log('> Ignore:', url);
// });

// generator.on('add', (url) => {
//     console.log('> Url:', url);
// });

// // register event listeners
// generator.on('done', () => {
//     console.log('> Done!:');
// });

// // start the crawler
// generator.start();


module.exports = sitemap;
